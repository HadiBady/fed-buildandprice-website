// gets the first element above our current with the name_of_element type
// or null is returned if that element does not exist
function above_element(element, name_of_element){
	name_of_element = name_of_element.toLowerCase();

	while(element && element.parentNode){
		element = element.parentNode;
		if(element.tagName && element.tagName.toLowerCase() == name_of_element){
			return element;
		}
	}
	return null;
}

var xmlhttp = null

if (window.XMLHttpRequest) {
    // code for modern browsers
    xmlhttp = new XMLHttpRequest();
	console.log("thisone");
 } else {
    // code for old IE browsers
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
} 
console.log(xmlhttp);
xmlhttp.open('GET','https://api.themoviedb.org/3/movie/popular?api_key=ae40e4997a55295b60c6768fd6b8c6c4&language=en-US&page=1',true)
console.log(xmlhttp);

console.log(xmlhttp.status);

xmlhttp.send();
// good resource https://code-boxx.com/wp-content/uploads/2019/06/js-ajax.jpg
// https://code-boxx.com/vanilla-javascript-ajax/
// https://thisinterestsme.com/fix-unexpected-token-o-in-json/
xmlhttp.onload = function(){
	var stringified = JSON.stringify(xmlhttp);
	var data = JSON.parse(stringified);
	console.log(this.status);
	console.log(this.response);
	console.log(data);
}

/////https://api.themoviedb.org/3/movie/popular?api_key=ae40e4997a55295b60c6768fd6b8c6c4&language=en-US&page=1

/*
	URL = https://api.themoviedb.org/3/movie/
	topic=popular?
	api_key=api_key=ae40e4997a55295b60c6768fd6b8c6c4
	right after everything else = &language=en-US&page=1
	
	https://api.themoviedb.org/3/discover/movie?api_key=ae40e4997a55295b60c6768fd6b8c6c4&sort_by=popularity.desc

*/

document.addEventListener('click', function (event) {

	// Only run if the clicked link was an accordion toggle
	if ( !event.target.classList.contains('on_click') ) {
		console.log("broken");
	return;}

	console.log(event);
	// Get the target content
	var content = event.target.id;
	if ( !content ) {
		console.log("broken2");
		return;
	}
	console.log(content);
	// Prevent default link behavior
	event.preventDefault();
	/* 43 top 21 bottom
	// If the content is already expanded, collapse it and quit
	if ( content.classList.contains('active') ) {
		content.classList.remove('active');
		return;
	}

	// Get all accordion content, loop through it, and close it
	var accordions = document.querySelectorAll('.accordion');
	for (var i = 0; i < accordions.length; i++) {
		accordions[i].classList.remove('active');
	}
	
	// Open our target content area
	content.classList.add('active');
	*/
}, false);
